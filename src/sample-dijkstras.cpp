#include <iomanip>
#include <iostream>
#include <limits>
#include <queue>

struct node {
    unsigned    idx;
    unsigned    dist;

    node()
        : idx( std::numeric_limits<unsigned>::max() ),
          dist( std::numeric_limits<unsigned>::max() )
    {}

    node( unsigned idx, unsigned dist )
        : idx( idx ),
          dist( dist )
    {}

    bool operator>( node const rhs ) const
    {
        return this->dist > rhs.dist;
    }
};

int main()
{
    unsigned    arr[6][6] = {};
    for( int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i ) {
        for( int j = 0; j < sizeof(arr[0]) / sizeof(arr[0][0]); ++j ) {
            arr[i][j] = std::numeric_limits<unsigned>::max();
        }
    }

    arr[0][1] = 2;
    arr[1][0] = 2;
    arr[0][2] = 7;
    arr[2][0] = 7;
    arr[0][3] = 4;
    arr[3][0] = 4;
    
    arr[1][2] = 9;
    arr[2][1] = 9;
    arr[1][4] = 1;
    arr[4][1] = 1;

    arr[2][3] = 10;
    arr[3][2] = 10;
    arr[2][4] = 6;
    arr[4][2] = 6;

    arr[2][5] = 6;
    arr[5][2] = 6;

    arr[3][5] = 1;
    arr[5][3] = 1;

    arr[4][5] = 3;
    arr[5][4] = 3;

    std::cout << "arr:" << std::endl;
    for( int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i ) {
        for( int j = 0; j < sizeof(arr[0]) / sizeof(arr[0][0]); ++j ) {
            std::cout << std::hex << std::setfill('0') << std::setw(8);
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }

    unsigned    pred[6] = {};
    for( int i = 0; i < sizeof(pred) / sizeof(pred[0]); ++i ) {
        pred[i] = std::numeric_limits<unsigned>::max();
    }

    unsigned    dist[6] = {};
    for( int i = 0; i < sizeof(dist) / sizeof(dist[0]); ++i ) {
        dist[i] = std::numeric_limits<unsigned>::max();
    }

    unsigned    visited[6] = {};
    for( int i = 0; i < sizeof(visited) / sizeof(visited[0]); ++i ) {
        visited[i] = false;
    }

    unsigned start = 1;
    unsigned end = 5;

    dist[start] = 0;

    std::priority_queue< node, std::vector<node>, std::greater<node> >  pq;
    pq.push( node( start, 0 ) );
    while( !pq.empty() && dist[end] == std::numeric_limits<unsigned>::max() ) {
        node node_cur = pq.top();
        unsigned cur = node_cur.idx;
        pq.pop();

        if( visited[cur] ) continue;

        visited[cur] = true;

        for( int i = 0; i < 6; ++i ) {
            if( i == cur ) continue;
            if( arr[cur][i] != std::numeric_limits<unsigned>::max() ) {
                if( dist[cur] + arr[cur][i] < dist[i] ) {
                    dist[i] = dist[cur] + arr[cur][i];
                    pred[i] = cur;
                }
                if( !visited[i] ) {
                    pq.push( node( i, dist[i] ) );
                }
            }
        }
    }

    if( dist[end] == std::numeric_limits<unsigned>::max() ) {
        std::cout << "No route found!" << std::endl;
    }

    std::cout << "route:" << std::endl;
    unsigned cur = end;
    std::cout << cur;
    cur = pred[cur];
    while( cur != std::numeric_limits<unsigned>::max() ) {
        std::cout << " <- " << cur;
        cur = pred[cur];
    }
    std::cout << std::endl;
}
