#include <cstdlib>
#include <cstring>
#include <iostream>

#include <time.h>

void Insertionsort( unsigned * arr, unsigned len )
{
    if( len <= 1 ) {
        return;
    }

    for( int i = 0; i < len-1; ++i ) {
        unsigned min_idx = i;
        for( int j = i+1; j < len; ++j ) {
            if( arr[j] < arr[min_idx] ) {
                min_idx = j;
            }
        }
        unsigned tmp = arr[i];
        arr[i] = arr[min_idx];
        arr[min_idx] = tmp;
    }
}

// TODO: SORT METHOD

int main()
{
    srand( time( NULL ) );
    
    size_t  IN_MAX = 99;
    size_t  IN_LEN = 16;
    
    unsigned in[IN_LEN] = {};
    unsigned verification[IN_LEN] = {};
    unsigned out[IN_LEN] = {};
    
    for( int i = 0; i < IN_LEN; ++i ) {
        in[i] = rand() % (IN_MAX+1);
    }

    memcpy( verification, in, IN_LEN * sizeof(in[0]) );
    Insertionsort( verification, IN_LEN );

    std::cout << "In:";
    for( int i = 0; i < IN_LEN; ++i ) {
        std::cout << " " << in[i];
    }
    std::cout << std::endl;

    // TODO: SORT
    
    // TODO: remove if sort operates in-place
    memcpy( out, in, IN_LEN * sizeof(in[0]) );

    std::cout << "Out:";
    for( int i = 0; i < IN_LEN; ++i ) {
        std::cout << " " << out[i];
    }
    std::cout << std::endl;

    if( memcmp( out, verification, IN_LEN * sizeof(in[0]) ) != 0 ) {
        std::cout << "Out doesn't match verification with Insertsort! should be:" << std::endl;
        std::cout << "Out:";
        for( int i = 0; i < IN_LEN; ++i ) {
            std::cout << " " << verification[i];
        }
        std::cout << std::endl;
    }

    return 0;
}
