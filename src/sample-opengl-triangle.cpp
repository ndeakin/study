#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

// compile with:
//   g++ -g `pkg-config --static --libs glfw3 glew` -lGL main.cpp

const GLchar * vertex_source = R"glsl(
    #version 150 core
    
    in vec2 position;
    in vec3 color;

    out vec3 vert_color;

    void main()
    {
        vert_color = color;
        gl_Position = vec4( position, 0.0, 1.0 );
    }
)glsl";

const GLchar * fragment_source = R"glsl(
    #version 150 core

    in vec3 vert_color;  

    out vec4 out_color;

    void main()
    {
        out_color = vec4( vert_color, 1.0 );
    }
)glsl";

int main()
{
    glfwInit();

    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE );
    glfwWindowHint( GLFW_RESIZABLE, GLFW_FALSE );

    GLFWwindow *    window = glfwCreateWindow( 800, 600,
                                             "OPenGL window",
                                             NULL, NULL );
    glfwMakeContextCurrent( window );

    glewExperimental = GL_TRUE;
    glewInit();

    float vertices[] = {
         0.0f,  0.5f, 1.0f, 0.0f, 0.0f, // vertex 1: red
         0.5f, -0.5f, 0.0f, 1.0f, 0.0f, // vertex 2: green
        -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // vertex 3: blue
    };

    GLuint vbo;
    glGenBuffers( 1, &vbo );
    glBindBuffer( GL_ARRAY_BUFFER, vbo );
    glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW );

    GLuint vao;
    glGenVertexArrays( 1, &vao );
    glBindVertexArray( vao );


    GLuint vertex_shader = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( vertex_shader, 1, &vertex_source, NULL );
    glCompileShader( vertex_shader );

    GLuint fragment_shader = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( fragment_shader, 1, &fragment_source, NULL );
    glCompileShader( fragment_shader );

    GLuint shader_program = glCreateProgram();
    glAttachShader( shader_program, vertex_shader );
    glAttachShader( shader_program, fragment_shader );

    glBindFragDataLocation( shader_program, 0, "out_color" );
    glLinkProgram( shader_program );
    glUseProgram( shader_program );

    GLint pos_attrib = glGetAttribLocation( shader_program, "position" );
    glVertexAttribPointer( pos_attrib, 2, GL_FLOAT, GL_FALSE,
                           5*sizeof(float), 0 );
    glEnableVertexAttribArray( pos_attrib );

    GLint col_attrib = glGetAttribLocation( shader_program, "color" );
    glVertexAttribPointer( col_attrib, 3, GL_FLOAT, GL_FALSE,
                           5*sizeof(float), (void*)(2*sizeof(float)) );
    glEnableVertexAttribArray( col_attrib );

    while( !glfwWindowShouldClose( window ) ) {
        glfwSwapBuffers( window );
        glfwPollEvents();

        if( glfwGetKey( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS ) {
            glfwSetWindowShouldClose( window, GL_TRUE );
        }

        glDrawArrays( GL_TRIANGLES, 0, 3 );
    }

    glfwDestroyWindow( window );
    glfwTerminate();
}
